#!/bin/bash
ENV=${1:-test}

#define the function to return the datat from the input file
function parse
{
    grep "${1}" ${ENV}.properties | cut -d'=' -f2
}

function is_success
{
	if [ $? -eq 0 ]
	then
		echo "[SUCCESSFUL]"
	else
		echo "[KO]"
	fi
}

function running_kafka
{
	kafka_connection="$(parse 'kafka_connection')"
	topic="$(parse 'topic')"
	if [ $kafka_connection -a $topic ]
	then
		echo "[OK]"
	else
		echo "[KO]"
		exit 1
	fi
}

function usage
{
	echo "[KO]"
    echo "USAGE:			./script PROPERTIES [DATA] [-m [N]]"
    echo "PROPERTIES 		is the properties path file without the .properties"
    echo "                  , which is the needed file's extention."
    echo "DATA 				is the file path containing the data to inject into kafka."
    echo "OPTION   -m N 	:manually inject N identics lines into kafka."
}

echo "Checking input validity..."
#CHECK VALIDITY OF THE INPUT PARAMETERS
if [ ! -r ${ENV}.properties ]
then
	echo "[KO]"
    echo "ERROR:	Property file is missing or not readable."
    exit 1
fi
if [ "$#" -lt 2 -o "$#" -gt 3 ]
then
	usage
    exit 1
fi

if [ "$#" -eq 2 -a $2 = '-m' ]
then
	usage
    exit 1
fi

if [ "$#" -eq 2 -a $2 != "-m" ]
then
		if [ ! -r $2 ]
		then
			echo "[KO]"
		    echo "ERROR:	DATA file is missing or not readable."
		    exit 1
		fi
		echo "[OK]"
		echo "Parsing '${ENV}.properties' property file..."
		running_kafka
		echo "Running the Kafka producer."
		docker run --rm --interactive ches/kafka kafka-console-producer.sh --broker-list $kafka_connection --topic $topic < $2
		is_success
		exit 0
fi

#check property file first
#check -m option
if [ "$#" -eq 3 -a $2 != "-m" ]
then
	echo "[KO]"
    echo "ERROR:	Unknown OPTION."
    exit 1
fi

if [ "$#" -eq 3 -a $2 = "-m" -a $3 -lt 0 ]
then
	echo "[KO]"
    echo "ERROR:	-m option need a positive N argument."
    exit 1
fi

if [ "$#" -eq 3 -a $2 = "-m" -a $3 -ge 0 ]
then
	#Run the producer with the given data
	echo "[OK]"
	echo "Generating $3 lines..."
	INPUT='{ "metadata": { "eventTechId": "TEST_JHKHK565", "version": "0.1", "timestamp": "2018-02-01T14:02:44.579Z", "deviceType": "passed through from channel", "browserType": "passed through from channel", "countryLanguage": "passed through from channel", "connectedUserRef": "passed from channel", "endUserRef": "passed from channel", "connectionRef": "passed from channel", "channelType": "passed from channel", "eventType": "AuthorisePayment", "serviceId": "passed from channel", "stageName": "passed from channel", "stageId": "passed from channel", "correlationId": "TEST14354", "bankCode": "BDDF", "channelCode": "Online", "originatingSystem": "OM", "destinationSystem": "IL", "priority": "1", "transactionFlow": "SCTOUTBOUND", "transactionId": "endToEndTransactionId_01", "paymentId": "paymentId_01" }, "payload": { "authorisePayment": { "party": { "accountDetails": { "iban": "DE88197040760865092652", "bic": "OYCLDKS0" } }, "amount": { "currency": "EUR", "value": "10.00" } } } }'
	for (( i = 0; i < $3; i++ )); do
			INPUTS+=$INPUT
	done
	echo "[OK]"
	echo "Running the Kafka producer."
	running_kafka
	echo $INPUT | docker run --rm --interactive ches/kafka kafka-console-producer.sh --broker-list $kafka_connection --topic $topic
	is_success
	exit 0
fi
