docker run --rm ches/kafka kafka-topics.sh --create --topic stress_test --replication-factor 1 --partitions 1 --zookeeper 172.17.0.2:2181


docker run --rm ches/kafka kafka-console-consumer.sh --topic stress_test --from-beginning --zookeeper 172.17.0.2:2181  --bootstrap-server

docker run --rm ches/kafka kafka-console-consumer.sh --new-consumer --topic stress_test --from-beginning --bootstrap-server localhost:9092

docker run --rm --interactive ches/kafka kafka-console-producer.sh --topic stress_test --broker-list 172.17.0.3:9092




TUTO FOR TESTING KAFKA using the script 

#Run Docker 
Set up the docker composer file :
zookeeper:
  image: zookeeper
  container_name: zookeeper
  ports:
   - 2181:2181
kafka:
  image: ches/kafka
  container_name: kafka
  links:
   - zookeeper
  ports:
   - 9092:9092
  environment:
   - KAFKA_AUTO_CREATE_TOPICS_ENABLE=true
   - ZOOKEEPER_IP=zookeeper
   - KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181
   - ADVERTISED_HOST=localhost
   - ADVERTISED_PORT=9092 

#Run
docker-composer up  (have to be within the docker-composer's directory)

#Run consumer

																																Zookeeper
docker run --rm ches/kafka kafka-console-consumer.sh --new-consumer --topic stress_test --from-beginning --bootstrap-server 172.17.0.3:9092

#RUn Producer 																		Zookeeper
 docker run --rm --interactive ches/kafka kafka-console-producer.sh --broker-list 172.17.0.3:9092 --topic stress_test

 #Run the script 